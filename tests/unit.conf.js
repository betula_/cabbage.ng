
module.exports = function(config) {
    config.set({
        frameworks: ['jasmine'],
        basePath: '../',
        files: [
            'tests/**/*.js'
        ],
        logLevel: config.LOG_INFO,
        browsers: ['Chrome'],
        reporters: ['progress'],

        singleRun: true,
        colors: true
    });
};
