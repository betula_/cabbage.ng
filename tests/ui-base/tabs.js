'use strict';

describe('tabs', function () {

    beforeEach(module('cabbage.uiBase'));

    describe('directive', function() {

        var $scope, $compile;
        beforeEach(inject(function ($rootScope, _$compile_) {
            $scope = $rootScope.$new();
            $compile = _$compile_;
        }));
        afterEach(function () {
            $scope.$destroy();
        });

        beforeEach(inject(function($animate) {
            $animate.__addClass = $animate.addClass;
            $animate.addClass = function(element, className, done) {
                element.addClass(className);
                done && done();
            };
            $animate.__removeClass = $animate.removeClass;
            $animate.removeClass = function(element, className, done) {
                element.removeClass(className);
                done && done();
            };
            $animate.__leave = $animate.leave;
            $animate.leave = function(element, done) {
                element.remove();
                done && done();
            };
        }));

        afterEach(inject(function($animate) {
            $animate.addClass = $animate.__addClass;
            $animate.removeClass = $animate.__removeClass;
            $animate.leave = $animate.__leave;
        }));


        var makeElement = function() {
            var element = $compile('<div tabs="model">' +
                '<div tab="">tab1</div>' +
                '<div tab="" class-active="my-active">tab2</div>' +
                '<div tab="my-value">tab3</div>' +
                '<div tab-pane="">pane1</div>' +
                '<div tab-pane="">pane2<div my-attribute=""></div></div>' +
                '<div tab-pane="my-value">pane3</div>' +
                '</div>')($scope);
            $scope.$apply();
            return element;
        };

        it('should active first on default', function () {
            var element = makeElement();

            var tabs = element[0].querySelectorAll('[tab]');
            expect(tabs.length).toBe(3);
            expect(tabs[0].className).toBe('active');
            expect(tabs[1].className).toBe('');
            expect(tabs[2].className).toBe('');

            var panes = element[0].querySelectorAll('[tab-pane]');
            expect(panes.length).toBe(1);
            expect(panes[0].innerText).toBe('pane1');
        });

        describe('should correctly change current', function() {
            var element;

            beforeEach(function() {
                element = makeElement();
            });

            it('via model', function() {
                $scope.model = 'my-value';
                $scope.$apply();

                var tabs = element[0].querySelectorAll('[tab].active');
                expect(tabs.length).toBe(1);
                expect(tabs[0].innerText).toBe('tab3');

                var panes = element[0].querySelectorAll('[tab-pane]');
                expect(panes.length).toBe(1);
                expect(panes[0].innerText).toBe('pane3');
            });

            it('via click', function() {
                angular.element(element[0].querySelector('[tab]:nth-child(2)')).click();

                var tabs = element[0].querySelectorAll('[tab]');
                expect(tabs.length).toBe(3);
                expect(tabs[0].className).toBe('');
                expect(tabs[1].className).toBe('my-active');
                expect(tabs[2].className).toBe('');

                var panes = element[0].querySelectorAll('[tab-pane]');
                expect(panes.length).toBe(1);
                expect(panes[0].innerHTML).toBe('pane2<div my-attribute=""></div>');

                expect($scope.model).toBe(2);

                $scope.model = 1;
                $scope.$apply();

                expect(element[0].querySelector('[tab]:nth-child(1).active')).toBeTruthy();
                expect(element[0].querySelector('[tab-pane]').innerText).toBe('pane1');

            });

        });



    });


    describe('animation', function() {
        beforeEach(module('mock.animate'));

        var animateFlush = function() {
            $animate.queue.forEach(function(tick) {
                tick.fn();
            });
            $animate.queue = [];
        };

        var $scope, $compile, $animate;
        beforeEach(inject(function ($rootScope, _$compile_, _$animate_) {
            $scope = $rootScope.$new();
            $compile = _$compile_;
            $animate = _$animate_;
        }));
        afterEach(function () {
            $scope.$destroy();
            animateFlush();
        });

        var makeElement = function() {
            var element = $compile('<div tabs="model">' +
                '<div tab="" value="a">tab1</div>' +
                '<div tab="b">tab2</div>' +
                '<div tab-pane="a">pane1</div>' +
                '<div tab-pane="" value="b">pane2</div>' +
                '</div>')($scope);
            $scope.$apply();
            return element;
        };

        it('should correctly change current', function() {
            makeElement();

            expect($animate.flushNext('addClass').element.text()).toBe('tab1');
            expect($animate.flushNext('enter').element.text()).toBe('pane1');

            $scope.model = 'b';
            $scope.$apply();

            expect($animate.flushNext('removeClass').element.text()).toBe('tab1');
            expect($animate.flushNext('addClass').element.text()).toBe('tab2');
            expect($animate.flushNext('leave').element.text()).toBe('pane1');
            expect($animate.flushNext('enter').element.text()).toBe('pane2');

        });

    });

});
