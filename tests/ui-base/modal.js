'use strict';

describe('modal', function () {

    beforeEach(module('cabbage.uiBase'));

    describe('directive', function() {

        var $scope, $compile, modalManager;
        beforeEach(inject(function ($rootScope, _$compile_, _modalManager_) {
            $scope = $rootScope.$new();
            $compile = _$compile_;
            modalManager = _modalManager_;
        }));
        afterEach(function () {
            $scope.$destroy();
        });

        beforeEach(inject(function($animate) {
            $animate.__addClass = $animate.addClass;
            $animate.addClass = function(element, className, done) {
                element.addClass(className);
                done && done();
            };
            $animate.__removeClass = $animate.removeClass;
            $animate.removeClass = function(element, className, done) {
                element.removeClass(className);
                done && done();
            };
            $animate.__leave = $animate.leave;
            $animate.leave = function(element, done) {
                element.remove();
                done && done();
            };
        }));

        afterEach(inject(function($animate) {
            $animate.addClass = $animate.__addClass;
            $animate.removeClass = $animate.__removeClass;
            $animate.leave = $animate.__leave;
        }));

        beforeEach(function() {
            modalManager.$rootBackdrops = [];
            modalManager.$rootContainers = [];
        });

        describe('use normal backdrop',function() {

            var makeElement = function() {
                var element = $compile('<div modal-container="">' +
                    '<div>[Before]</div>' +
                    '<div modal-backdrop="">[Backdrop]</div>' +
                    '<div last-element="">[After]</div>' +
                    '</div>')($scope);
                $scope.$apply();
                return element;
            };

            it('should register and remove backdrop from DOM', function () {
                var element = makeElement();
                expect(element[0].querySelector('[modal-backdrop]')).toBeFalsy();
                expect(modalManager.$rootContainers.length).toBe(1);
                expect(modalManager.$rootContainers[0].backdrops.length).toBe(1);

            });

            it('should create modal and use backdrop', function() {
                var element = makeElement();
                modalManager.show('SignIn');
                expect(element[0].querySelector('[modal-backdrop]')).toBeTruthy();
                expect(element[0].querySelector('[sign-in]')).toBeTruthy();
                expect(element[0].querySelector('[last-element]').nextSibling.hasAttributes('sign-in')).toBeTruthy();
            });

            describe('should close modal and backdrop', function() {
                var element;

                beforeEach(function() {
                    element = makeElement();
                    var params = {
                        fn: function() {
                            this.close();
                        }
                    };
                    modalManager.show('MyModal', params);
                });

                it('possible', function() {
                    expect(element[0].querySelector('[modal-backdrop]')).toBeTruthy();
                    expect(element[0].querySelector('[my-modal]')).toBeTruthy();
                });

                it('throw modal scope close function', function() {
                    var scope = angular.element(element[0].querySelector('[my-modal]')).scope();
                    scope.fn();
                    expect(element[0].querySelector('[modal-backdrop]')).not.toBeTruthy();
                    expect(element[0].querySelector('[my-modal]')).not.toBeTruthy();
                });

                it('throw backdrop click', function() {
                    angular.element(element[0].querySelector('[modal-backdrop]')).click();
                    expect(element[0].querySelector('[modal-backdrop]')).not.toBeTruthy();
                    expect(element[0].querySelector('[my-modal]')).not.toBeTruthy();
                });

            });
        });

        describe('use root backdrop', function() {
            var makeElement = function() {
                var element = $compile('<div>' +
                    '<div modal-container="">' +
                    '</div>' +
                    '<div modal-backdrop="">[Backdrop]</div>' +
                    '</div>')($scope);
                $scope.$apply();
                return element;
            };

            it('should register backdrop', function() {
                var element = makeElement();
                expect(element[0].querySelector('[modal-backdrop]')).toBeFalsy();
                expect(modalManager.$rootBackdrops.length).toBe(1);
                expect(modalManager.$rootContainers.length).toBe(1);
            });

            describe('should close modal and backdrop', function() {
                var element;

                beforeEach(function() {
                    element = makeElement();
                    modalManager.show('MyModal');
                });

                it('possible', function() {
                    expect(element[0].querySelector('[modal-backdrop]')).toBeTruthy();
                    expect(element[0].querySelector('[my-modal]')).toBeTruthy();
                });

                it('throw modal scope close function', function() {
                    var scope = angular.element(element[0].querySelector('[my-modal]')).scope();
                    scope.close();
                    expect(element[0].querySelector('[modal-backdrop]')).not.toBeTruthy();
                    expect(element[0].querySelector('[my-modal]')).not.toBeTruthy();
                });

                it('throw backdrop click', function() {
                    angular.element(element[0].querySelector('[modal-backdrop]')).click();
                    expect(element[0].querySelector('[modal-backdrop]')).not.toBeTruthy();
                    expect(element[0].querySelector('[my-modal]')).not.toBeTruthy();
                });

            });
        });
    });


    describe('animation', function() {
        beforeEach(module('mock.animate'));

        var animateFlush = function() {
            $animate.queue.forEach(function(tick) {
                tick.fn();
            });
            $animate.queue = [];
        };

        var $scope, $compile, $animate;
        beforeEach(inject(function ($rootScope, _$compile_, _$animate_) {
            $scope = $rootScope.$new();
            $compile = _$compile_;
            $animate = _$animate_;
        }));
        afterEach(function () {
            $scope.$destroy();
            animateFlush();
        });
    });

});
