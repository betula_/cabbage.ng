'use strict';

describe('toggle', function () {

    beforeEach(module('cabbage.uiBase'));

    var makeElement = function() {
        var element = $compile('<div toggle="model.value"></div>')($scope);
        $scope.$apply();
        return element;
    };

    var $scope, $compile, $element;

    beforeEach(inject(function ($rootScope, _$compile_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $element = makeElement();
    }));
    afterEach(function () {
        $scope.$destroy();
        $element.remove();
    });

    it('should work', function() {
        $element.trigger('click');
        expect($scope.model).toBeTruthy();
        expect($scope.model.value).toBe(true);
        $element.trigger('click');
        expect($scope.model.value).toBe(false);
    });

    it('should use model value', function() {
        $scope.model = { value: true };
        $element.trigger('click');
        expect($scope.model.value).toBe(false);
    });

});

describe('hover', function() {

    beforeEach(module('cabbage.uiBase'));

    var makeElement = function() {
        var element = $compile('<div hover="model.value"></div>')($scope);
        $scope.$apply();
        return element;
    };

    var $scope, $compile, $element;

    beforeEach(inject(function ($rootScope, _$compile_) {
        $scope = $rootScope.$new();
        $compile = _$compile_;
        $element = makeElement();
    }));
    afterEach(function () {
        $scope.$destroy();
        $element.remove();
    });


    it('should work', function() {
        $element.trigger('mouseover');
        expect($scope.model).toBeTruthy();
        expect($scope.model.value).toBe(true);
        $element.trigger('mouseout');
        expect($scope.model.value).toBe(false);
    });

    it('should use model value', function() {
        $scope.model = { value: true };
        $element.trigger('mouseover');
        expect($scope.model.value).toBe(true);
        $element.trigger('mouseover');
        expect($scope.model.value).toBe(true);
        $element.trigger('mouseout');
        expect($scope.model.value).toBe(false);
    });

});