
Module.directive('form', [function() {
  return {
    restrict: 'E',
    priority: 100,
    compile: function(element, attr) {
      var name = attr.name || attr.ngForm;
      if (!name) {
        attr.$set('name', 'form');
      }
    }
  }
}]);
