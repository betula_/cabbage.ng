
Module.directive('pressEnter', ['$parse', function($parse) {
  return {
    restrict: 'A',
    link: function (scope, element, attr) {
      var action = $parse(attr.pressEnter);
      element.bind('keydown', function(e) {
        if (e.keyCode === 13) {
          scope.$apply(function() {
            action(scope);
          });
        }
      });
    }
  }
}]);
