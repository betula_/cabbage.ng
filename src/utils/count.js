
Module.filter('count', [function() {

  return function(data){
    return data ? (data.length || 0) : 0;
  };

}]);