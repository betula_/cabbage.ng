
Module.controller('modalContainerDirective', ['$animate', '$compile', '$element', '$scope', '$modalManager', function($animate, $compile, $element, $scope, $modalManager) {
  var getDashedName = function(name) {
    name && (name = name[0].toLowerCase() + name.substring(1));
    return name.replace(/([a-z0-9])([A-Z])/gm, function(match, a, b) {
      return a+'-'+b;
    });
  };
  this.modals = {};
  this.defaultBackdropName = null;
  this.modal = function(name, params, backdropName) {
    params = params || {};
    if ((typeof params.single == 'undefined' || !params.single) && this.modals[name]) return;

    var self = this;
    var backdrop = $modalManager.getBackdrop(backdropName, this.defaultBackdropName);
    var modalElement = $compile('<modal '+getDashedName(name)+'="" />')($scope);
    var modalScope = modalElement.scope();
    var modal = {
      close: function() {
        var deferred = jQuery.Deferred();
        $animate.leave(modalElement, function() {
          deferred.resolve();
        });
        self.modals[name] = false;
        return deferred.promise();
      }
    };
    modalScope.$modalClose = function() {
      $animate.leave(modalElement, function() {
        backdrop.detachModal(modal);
      });
      self.modals[name] = false;
    };
    modalScope.$modalContainer = this.name;
    modalScope.$modalBackdrop = backdrop.name;
    var i;
    for(i in params) {
      if (params.hasOwnProperty(i)) {
        modalScope[i] = params[i];
      }
    }
    backdrop.attachModal(modal);

    var element = $scope.$$_transcludedElement || $element;
    $animate.enter(modalElement, element);
    this.modals[name] = true;
  };

}]);

Module.directive('modalContainer', ['$modalManager', function($modalManager) {
  var KEY_DEFAULT = '';
  return {
    require: 'modalContainer',
    restrict: 'AE',
    priority: 550,
    link: function(scope, element, attr, container) {
      var name = attr.modalContainer || attr.name || KEY_DEFAULT;
      container.defaultBackdropName = attr.modalBackdrop || attr.modalBackdropDefault;
      container.name = name;
      $modalManager.containers[name] = container;
      scope.$on('$destroy', function() {
        if ($modalManager.containers[name] === container) {
          delete $modalManager.containers[name];
        }
      });
    },
    controller: 'modalContainerDirective'
  }
}]);

Module.directive('modalBackdrop', ['$modalManager', '$animate', function($modalManager, $animate) {
  var KEY_DEFAULT = '';
  var BackdropController = function($scope, $element, $transclude) {
    var childElement, childScope;
    var closed = true;
    var closing = false;
    var self = this;
    this.modals = [];
    this.show = function() {
      if (!closed) {
        if (closing) closing = false;
        return;
      }
      closed = false;
      childScope = $scope.$new();
      $transclude(childScope, function (clone) {
        $scope.$$_transcludedElement = childElement = clone;
        $animate.enter(clone, $element.parent(), $element);
        childElement.on('click', function() {
          self.close();
        });
      });
    };

    this.close = function() {
      if (closed || closing) return;
      closing = true;
      var promises = [];
      var self = this;
      angular.forEach(this.modals, function(modal) {
        var promise = modal.close();
        promise.then(function() {
          for (var i = 0; i < self.modals.length; ) {
            if (self.modals[i] === modal) {
              self.modals.splice(i, 1);
            } else i++;
          }
        });
        promises.push(promise);
      });
      if (!promises.length) _close();
      else {
        jQuery.when(promises).then(_close, function() {
          closing = false;
        });
      }
      function _close() {
        setTimeout(function() {
          if (closing) _hide();
        }, 0);
      }
      function _hide() {
        if (childElement) {
          $animate.leave(childElement);
          $scope.$$_transcludedElement = childElement = undefined;
        }
        if (childScope) {
          childScope.$destroy();
          childScope = undefined;
        }
        closed = true;
        closing = false;
      }
    };
    this.attachModal = function(modal) {
      this.modals.push(modal);
      if (this.modals.length == 1) {
        this.show();
      }
    };
    this.detachModal = function(modal) {
      for (var i = 0; i < this.modals.length; ) {
        if (this.modals[i] === modal) {
          this.modals.splice(i, 1);
        } else ++i;
      }
      if (!this.modals.length) {
        this.close();
      }
    };
  };

  return {
    transclude: 'element',
    priority: 500,
    restrict: 'A',
    compile: function (element, attr, transclude) {
      return function(scope, element, attr) {
        var backdrop = new BackdropController(scope, element, transclude);
        var name = attr.modalBackdrop || KEY_DEFAULT;
        backdrop.name = name;
        $modalManager.backdrops[name] = backdrop;
        scope.$on('$destroy', function() {
          if ($modalManager.backdrops[name] === backdrop) {
            delete $modalManager.backdrops[name];
          }
          backdrop = undefined;
        });
      }
    }
  }
}]);

Module.factory('$modalManager', ['$controller', '$compile', function($controller, $compile) {
  var KEY_DEFAULT = '';

  return {
    containers: {},
    backdrops: {},

    getDefaultContainer: function() {
      var container = this.containers[KEY_DEFAULT];
      if (!container) {
        var body = angular.element(document.getElementsByTagName('body'));
        var scope = body.scope();
        container = $controller('modalContainerDirective', {
          $scope: scope,
          $element: body
        });
        body.data('$modalContainerDirectiveController', container);
        this.containers[KEY_DEFAULT] = container;
        var self = this;
        scope.$on('$destroy', function() {
          if (self.containers[KEY_DEFAULT] === container) {
            delete self.containers[KEY_DEFAULT];
          }
        });
      }
      return container;
    },
    getContainer: function(name) {
      return this.containers[name] || this.getDefaultContainer();
    },

    getDefaultBackdrop: function() {
      var backdrop = this.backdrops[KEY_DEFAULT];
      if (!backdrop) {
        var body = angular.element(document.getElementsByTagName('body'));
        var element = $compile('<div class="modal-backdrop" modal-backdrop=""></div>')(body.scope());
        body.append(element);
        backdrop = this.backdrops[KEY_DEFAULT];
        if (!backdrop) {
          throw new Error('Default backdrop can\'t be found');
        }
      }
      return backdrop;
    },
    getBackdrop: function(name) {
      var args = [].slice.call(arguments);
      for (var i = 0; i < args.length; i++) {
        if (this.backdrops[args[i]]) return this.backdrops[args[i]];
      }
      return this.getDefaultBackdrop();
    },

    show: function(name, params, containerName, backdropName) {
      this.getContainer(containerName).modal(name, params, backdropName);
    }
  };

}]);

Module.directive('modal', [function() {
  return {
    restrict: 'E',
    controller: ['$scope', function($scope) {
      this.close = function() {
        $scope.$modalClose();
      };
    }]
  }
}]);

Module.directive('clickModalShow', ['$modalManager', '$parse', function($modalManager, $parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      var prefix = 'modalParams';
      var l = prefix.length;
      var name, map = [];
      for (var i in attr) {
        if (attr.hasOwnProperty(i) && i.substring(0, l) == prefix) {
          name = i.substring(l);
          name = name[0].toLowerCase() + name.substring(1);
          map.push([name, $parse(attr[i])]);
        }
      }
      element.on('click', function() {
        scope.$apply(function() {
          var params = {};
          for (var i = 0; i < map.length; i++) {
            params[map[i][0]] = map[i][1](scope);
          }
          $modalManager.show(attr.clickModalShow, params, attr.modalShowContainer, attr.modalShowBackdrop);
        });
      });
    }
  }
}]);

Module.directive('clickModalClose', ['$parse', function($parse) {
  return {
    require: '^modal',
    link: function(scope, element, attr, modal) {
      element.on('click', function(e) {
        var defaultPrevented = false;
        if (attr.clickModalClose) {
          scope.$apply(function() {
            $parse(attr.clickModalClose)(scope, {
              $event: { preventDefault: function() { defaultPrevented = true; } }, $clickEvent: e
            });
          });
        }
        if (!defaultPrevented) modal.close();
      });
    }
  }
}]);


