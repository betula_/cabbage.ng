

Module.directive('hover', ['$parse', function($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      if (!attr.hover) {
        return;
      }
      var setter = $parse(attr.hover).assign;
      element.on('mouseover', function() {
        scope.$apply(function() {
          setter(scope, true);
        });
      });
      element.on('mouseout', function() {
        scope.$apply(function() {
          setter(scope, false);
        });
      });
    }
  }
}]);

Module.directive('toggle', ['$parse', function($parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attr) {
      if (!attr.toggle) {
        return;
      }
      var getter = $parse(attr.toggle);
      var setter = getter.assign;
      element.on('click', function() {
        scope.$apply(function() {
          var value = getter(scope);
          setter(scope, value ? false : true);
        });
      });
    }
  }
}]);