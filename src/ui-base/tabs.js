
Module.directive('tabs', ['$parse', function($parse) {
  return {
    restrict: 'A',
    require: 'tabs',
    link: function(scope, element, attr, tabs) {
      var exprModel = attr.model || attr.tabs;
      var defaultActive = attr.tabsDefault;

      if (defaultActive) {
        tabs.select(defaultActive);
      }
      if (exprModel) {
        var getter = $parse(exprModel);
        tabs.select = function(id) {
          getter.assign(scope, id);
        };
        var id = getter(scope);
        if (!id && tabs.active) {
          getter.assign(scope, tabs.active);
        }

        scope.$watch(exprModel, function(id) {
          tabs.makeActive(id);
        });
      }
    },
    controller: [function() {
      this.tabs = [];
      this.panes = [];

      this.tabUniqid = 0;
      this.paneUniqid = 0;

      this.active = null;

      this.select = function(id) {
        this.makeActive(id);
      };
      this.makeActive = function(id) {
        if (this.active == id) {
          return;
        }
        var i;
        for (i = 0; i < this.tabs.length; i++) {
          this.tabs[i].makeActive(this.tabs[i].id == id);
        }
        for (i = 0; i < this.panes.length; i++) {
          this.panes[i].makeActive(this.panes[i].id == id);
        }
        this.active = id;
      };

      this.attachTab = function(tab) {
        if (!tab.id) {
          tab.id = ++this.tabUniqid;
        }
        if (!this.active) {
          this.select(tab.id);
        }
        this.tabs.push(tab);
        tab.makeActive(tab.id == this.active);
      };
      this.detachTab = function(tab) {
        for (var i = 0; i < this.tabs.length; ) {
          if (this.tabs[i] === tab) {
            this.tabs.splice(i, 1);
          } else ++i;
        }
      };

      this.attachPane = function(pane) {
        if (!pane.id) {
          pane.id = ++this.paneUniqid;
        }
        if (!this.active) {
          this.select(pane.id);
        }
        this.panes.push(pane);
        pane.makeActive(pane.id == this.active);
      };
      this.detachPane = function(pane) {
        for (var i = 0; i < this.panes.length; ) {
          if (this.panes[i] === pane) {
            this.panes.splice(i, 1);
          } else ++i;
        }
      };


    }]
  }
}]);


Module.directive('tab', ['$animate', function($animate) {
  var TabController = function() {
    this.id = null;
    this.tabs = null;
    this.active = false;
    this.activators = [];
    this.select = function() {
      if (this.active) {
        return;
      }
      this.tabs.select(this.id);
    };
    this.makeActive = function(active) {
      if (this.active == active) {
        return;
      }
      this.active = active;
      for (var i = 0; i < this.activators.length; i++) {
        this.activators[i](active);
      }
    };
  };
  return {
    restrict: 'A',
    require: '^tabs',
    link: function(scope, element, attr, tabs) {
      var tab = new TabController();

      tab.id = attr.tab || attr.value || tab.id;
      tab.classActive = attr.tabActiveClass || 'active';
      tab.tabs = tabs;

      var doActive = function(active) {
        $animate[active ? 'addClass' : 'removeClass'](element, tab.classActive);
      };
      if (element.hasClass(tab.classActive) ^ tab.active) {
        doActive(tab.active);
      }

      tab.activators.push(doActive);
      tabs.attachTab(tab);

      scope.$on('$destroy', function() {
        tabs.detachTab(tab);
      });
      element.on('click', function() {
        scope.$apply(function() {
          tab.select();
        });
      });
    }
  }
}]);


Module.directive('tabPane', ['$animate', function($animate) {
  var PaneController = function() {
    this.id = null;
    this.active = false;
    this.activators = [];
    this.makeActive = function(active) {
      if (this.active == active) {
        return;
      }
      this.active = active;
      for (var i = 0; i < this.activators.length; i++) {
        this.activators[i](active);
      }
    };
  };

  return {
    transclude: 'element',
    priority: 500,
    restrict: 'A',
    require: '^tabs',

    compile: function (element, attr, transclude) {
      return function (scope, element, attr, tabs) {
        var pane = new PaneController();
        element.data('$tabPaneController', pane);

        pane.id = attr.tabPane || attr.value || pane.id;

        var childElement, childScope;
        var doActive = function(active) {
          if (childElement) {
            $animate.leave(childElement);
            childElement = undefined;
          }
          if (childScope) {
            childScope.$destroy();
            childScope = undefined;
          }
          if (active) {
            childScope = scope.$new();
            transclude(childScope, function (clone) {
              childElement = clone;
              $animate.enter(clone, element.parent(), element);
            });
          }
        };

        if (pane.active) {
          doActive(pane.active);
        }
        pane.activators.push(doActive);
        tabs.attachPane(pane);

        scope.$on('$destroy', function() {
          tabs.detachPane(pane);
        });
      }
    }
  }
}]);